﻿using System;

namespace comp5002_100105777_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            string username;
            string option;
            double num; 
            double gst;
            gst = 0.15;

            Console.Clear();
            Console.WriteLine("Welcome to Wheels on Deals");//Display "Welcome to Wheels on Deals"//




            Console.WriteLine("Enter Your username:");//Ask user to write their username//
            username = Console.ReadLine();


            Console.WriteLine($"Hi {username}, Welcome to the shop");//Display "Hi(username),Welcome to Wheels on Deals//
            Console.WriteLine("Write price of product with two decimal places");//Ask user to add price of product in decimals (two decimal places)//
            num = Convert.ToDouble(Console.ReadLine());


            Console.WriteLine("Would you like to add another product?");//Ask if the user wants to add another value//
            Console.WriteLine("Type 1 for yes or 2 for no");//Ask user to type 1 for yes or 2 for no//


            option = Console.ReadLine();
            if (option == "1")
            {



                Console.WriteLine("Enter price of other product");//Enter the value (two decimal places) of 2nd product//
                num += Convert.ToDouble(Console.ReadLine());



            }
            Console.WriteLine($"The total value is = {num+ (num*gst)}");
            Console.WriteLine("Thank You For Shopping with us, Please come again");//Display"Thank you for shopping with us, please come again"
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();


        }
    }
}
